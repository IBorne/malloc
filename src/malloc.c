#include <malloc.h>

extern struct page *page;

__attribute__((visibility("default")))
void *malloc(size_t size)
{
    size = word_align(size);
    if (!page)
        page = page_init(size);
    struct block *block = block_find(page, size);
    if (!block)
        block = block_add(page, size);
    block->free = 1;
    return block + 1;
}

__attribute__((visibility("default")))
void free(void *ptr)
{
    if (ptr)
    {
        struct page *f_p = NULL;
        struct block *block = block_find_free(page, ptr, &f_p);
        if (block)
        {
            if (block->free == 1)
                block->free = 0;
            else
                errx(1, "attempting double free");
        }
        if (f_p != page && page_count_block(f_p) == 0)
        {
            page_connect(page, f_p);
            page_free(f_p);
        }
    }
}

__attribute__((visibility("default")))
void *realloc(void *ptr, size_t size)
{
    if (!ptr)
        return malloc(size);
    struct block *block = block_find_free(page, ptr, NULL);
    size_t old_size = block->size;
    if (block)
    {
        if (block->size == size)
            return ptr;
        block->free = 0;
    }
    if (size == 0)
        return NULL;
    void *new = malloc(size);
    memmove(new, ptr, size < old_size ? size : old_size);
    return new;
}

__attribute__((visibility("default")))
void *calloc(size_t nmemb, size_t size)
{
    if (!ovf(nmemb, size))
        return NULL;
    void *ptr = malloc(size * nmemb);
    if (ptr)
        memset(ptr, 0, size * nmemb);
    return ptr;
}