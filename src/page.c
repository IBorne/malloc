#include <malloc.h>

struct page *page_init(size_t size)
{
    size_t new_size = PAGE_SIZE - sizeof(struct page) - sizeof(struct block);
    if (size < new_size)
        size = PAGE_SIZE;
    else
        size += sizeof(struct page) + sizeof(struct block);
    size = page_align(size);
    int prot = PROT_READ | PROT_WRITE;
    int flags = MAP_PRIVATE | MAP_ANONYMOUS;
    struct page *page = mmap(NULL, size, prot, flags, -1, 0);
    if (page == MAP_FAILED)
        errx(2, "Memory exhausted");
    page->next = NULL;
    page->capacity = size;
    page->size = sizeof(struct page);
    page->meta = NULL;
    return page;
}

static struct block *_block_add(struct page *page,\
        struct block *prev, size_t block_size)
{
    struct block *new = NULL;
    if (!prev)
        new = shift(page, sizeof(struct page));
    else
        new = shift(prev + 1, prev->size);
    new->security = 99999;
    new->next = NULL;
    new->free = 0;
    new->size = block_size;
    page->size += sizeof(struct block) + block_size;
    return new;
}

struct block *block_add(struct page *page, size_t block_size)
{
    if (!page || block_size == 0)
        return NULL;
    size_t new_size = page->size + sizeof(struct block) + block_size;
    if (new_size > page->capacity)
    {
        if (page->next)
            return block_add(page->next, block_size);
        page->next = page_init(block_size);
        page->next->meta = _block_add(page->next, NULL, block_size);
        return page->next->meta;
    }
    struct block *meta = page->meta;
    if (!meta)
    {
        page->meta = _block_add(page, NULL, block_size);
        return page->meta;
    }
    while (meta->next)
        meta = meta->next;
    meta->next = _block_add(page, meta, block_size);
    return meta->next;
}

struct block *block_find(struct page *page, size_t size)
{
    if (!page)
        return NULL;
    struct block *block = page->meta;
    while (block)
    {
        if (size <= block->size && block->free == 0)
            return block;
        block = block->next;
    }
    if (page->next)
        return block_find(page->next, size);
    return NULL;
}

struct block *block_find_free(struct page *page, void *ptr, struct page **f_p)
{
    if (!page || !ptr)
        return NULL;
    struct block *block = page->meta;
    while (block)
    {
        if (block + 1 == ptr || block + 2 == ptr)
        {
            if (f_p)
                *f_p = page;
            return block;
        }
        block = block->next;
    }
    if (page->next)
        return block_find_free(page->next, ptr, f_p);
    return NULL;
}

static int _page_count_block(struct block *block)
{
    if (block)
    {
        if (block->free == 0)
            return _page_count_block(block->next);
        return 1 + _page_count_block(block->next);
    }
    return 0;
}

int page_count_block(struct page *page)
{
    if (page)
    {
        int next_count = 0;
        if (page->next)
            next_count = page_count_block(page->next);
        return next_count + _page_count_block(page->meta);
    }
    return 0;
}

void page_connect(struct page *page_begin, struct page *f_p)
{
    while(page_begin->next && page_begin->next != f_p)
        page_begin = page_begin->next;
    if (!page_begin->next)
        return;
    page_begin->next = page_begin->next->next;
}

void page_free(struct page *page)
{
    munmap(page, page->capacity);
}