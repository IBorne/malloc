#include <malloc.h>

#define OVF(a, b) \
   __builtin_mul_overflow_p (a, b, (__typeof__ ((a) * (b))) 0)

size_t *to_size(void *ptr)
{
    size_t *pt= ptr;
    return pt;
}

void *to_void(void *ptr)
{
    return ptr;
}

inline size_t word_align(size_t n)
{
    size_t rest = n % 8;
    if (rest == 0)
        return n;
    return n +  8 - rest;
}

size_t page_align(size_t n)
{
    size_t rest = n % PAGE_SIZE;
    if (rest == 0)
        return n;
    return n + PAGE_SIZE - rest;
}

size_t ovf(size_t a, size_t b)
{
    return OVF(a, b) ? 0 : a * b;
}

void *shift(void *ptr, size_t offset)
{
    char *pt = ptr;
    return pt + offset;
}

void *page_begin(void *ptr, size_t page_size)
{
    size_t add = (size_t)ptr;
    char *res = ptr;
    size_t diff = page_size - 1;
    diff &= add;
    res -= diff;
    return res;
}
