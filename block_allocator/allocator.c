#include <stdlib.h>
#include <stddef.h>
#include <sys/mman.h>
#include <unistd.h>

#include "allocator.h"

struct blk_allocator *blka_new(void)
{
    struct blk_allocator *new = malloc(sizeof(struct blk_allocator));
    if (!new)
        return NULL;
    new->meta = NULL;
    return new;
}

static void _blka_delete(struct blk_meta *meta)
{
    if (meta)
    {
        _blka_delete(meta->next);
        munmap(meta, meta->size);
    }
}

void blka_delete(struct blk_allocator *blka)
{
    if (blka)
    {
        if (blka->meta)
            _blka_delete(blka->meta);
        free(blka);
    }
}

struct blk_meta *blka_alloc(struct blk_allocator *blka, size_t size)
{
    size_t page_size = sysconf(_SC_PAGESIZE);
    if (size < page_size)
        size = page_size;
    int prot = PROT_READ | PROT_WRITE;
    int flags = MAP_ANONYMOUS | MAP_PRIVATE;

    struct blk_meta *new = mmap(NULL, size, prot, flags, -1, 0);
    if (new == MAP_FAILED)
        return NULL;
    new->next = blka->meta;
    blka->meta = new;
    if (size < page_size)
        new->size = size - sizeof(struct blk_meta);
    else
        new->size = size;
    return new;
}

void blka_free(struct blk_meta *blk)
{
    if (blk)
        munmap(blk, blk->size);
}

void blka_pop(struct blk_allocator *blka)
{
    if (blka && blka->meta)
    {
        struct blk_meta *tmp = blka->meta;
        blka->meta = blka->meta->next;
        blka_free(tmp);
    }
}
