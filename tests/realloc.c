#include <stdio.h>
#include <stdlib.h>

void write_data(char *data, int size, char a)
{
    for (int i = 0; i < size; i++)
        data[i] = a + i;
}

int main()
{
    char *data = malloc(sizeof(int) * 5);
    write_data(data, 5, 'a');
    data = realloc(data, sizeof(int) * 10);
    write_data(data, 10, 'e');
    data = realloc(data, sizeof(int) * 20);
    write_data(data, 20, 'i');
    data = realloc(data, sizeof(int) * 30);
    write_data(data, 30, 'a');
    free(data);
}
