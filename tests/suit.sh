#!/bin/sh

test_file='file_tests'
malloc='LD_PRELOAD=./../libmalloc.so'

RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[0;33m'
NC='\033[0m'

test_minishell()
{
    while read line; do
        echo -e "${YELLOW}$line${NC}"

        $malloc $line 1>exe_out 2>exe_err
        exe_ret=$?
        $line 1>ref_out 2>ref_err
        ref_ret=$?

        if [ $exe_ret -eq $ref_ret ]; then
            echo -e "${GREEN}[RET] [GOOD]${NC}"
        else
            echo -e "${RED}[RET] [FAIL]${NC} expected: '$ref_ret', got : '$exe_ret'"
        fi

        v=$(diff -y exe_out ref_out)
        var=$?
        if [ $var -eq 0 ]; then
            echo -e "${GREEN}[OUT] [GOOD]${NC}"
        else
            echo -e "${RED}[OUT] [FAIL]${NC}"
            echo "$v"
        fi

        wc_exe_err=$(wc -l exe_err | cut -f1 -d' ')
        wc_ref_err=$(wc -l ref_err | cut -f1 -d' ')
        if [ "$wc_exe_err" -eq 0 ]; then
            if [ "$wc_ref_err" -eq 0 ]; then
                echo -e "${GREEN}[ERR] [GOOD]${NC}"
            else
                echo -e "${RED}[ERR] [FAIL]${NC}"
            fi
        else
            if [ "$wc_ref_err" -ne 0 ]; then
                echo -e "${GREEN}[ERR] [GOOD]${NC}"
            else
                echo -e "${RED}[ERR] [FAIL]${NC}"
            fi
        fi
    done < $test_file
    rm exe_out exe_err ref_out ref_err 
    return
}

main ()
{
    touch ref_err ref_out
    echo '____________________________________________________________________'
    test_minishell
    echo '____________________________________________________________________'
    echo
}
main