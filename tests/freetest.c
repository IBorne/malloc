#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define SIZE 4096

void *my_memcpy(void *dest, const void *src, size_t len)
{
    char *d = dest;
    const char *s = src;
    while (len--)
        *d++ = *s++;
    return dest;
}

char *my_strdup(const char *s)
{
    size_t len = strlen(s) + 1;
    void *new = malloc(len);
    if (!new)
        return NULL;
    return (char *)my_memcpy(new, s, len);
}

void mass_free(char **data)
{
    for (int i = 0; i < SIZE; i++)
        data[i] = my_strdup("Bonjour");
    for (int i = SIZE - 1; i >= 0; i--)
        free(data[i]);
}

int main(void)
{
    char **data = malloc(sizeof(char*) * SIZE);
    mass_free(data);
    free(data);
}
