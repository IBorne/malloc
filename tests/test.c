#include <stdio.h>
#include <stdlib.h>

#define SIZE 10
#define BIGSIZE 4100

static void write_data(int *data, size_t size)
{
    printf("data = %ld: ", sizeof(int) * BIGSIZE);
    for (size_t i = 0; i < size; i++)
        data[i] = i;
}

static void print_data(int *data, size_t size)
{

    printf("data = %ld: ", sizeof(int) * SIZE * 410);
    for (size_t i = 0; i < size; i += 99)
        printf("data[%ld] = %i,", i, data[i]);
    printf("\n");
}

int main(void)
{
    char *tab = malloc(sizeof(char) * SIZE);
    for (int i = 0; i < SIZE; i++)
        tab[i] = 'a' + i;
    printf("tab = %ld: %s", sizeof(char) * SIZE, tab);
    printf("\n");
    free(tab);

    int *data = malloc(sizeof(int) * BIGSIZE);
    write_data(data, BIGSIZE);
    print_data(data, BIGSIZE);
    free(data);

    int *data2 = malloc(sizeof(int) * BIGSIZE);
    write_data(data2, BIGSIZE);
    print_data(data2, BIGSIZE);

    int *data3 = malloc(sizeof(int) * BIGSIZE);
    write_data(data3, BIGSIZE);
    print_data(data3, BIGSIZE);
    free(data3);

    free(data2);
}
