#include <stdint.h>
#include <stddef.h>

void *page_begin(void *ptr, size_t page_size)
{
    size_t add = (size_t)ptr;
    char *res = ptr;
    size_t diff = page_size - 1;
    diff &= add;
    res -= diff;
    return res;
}
