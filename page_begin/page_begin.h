#ifndef PAGE_BEGIN_H
# define PAGE_BEGIN_H

void *page_begin(void *ptr, size_t page_size);

#endif /* !PAGE_BEGIN_H */
