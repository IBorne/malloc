#include <stddef.h>
#include "beware_overflow.h"

#define OVERFLOW(a, b) \
   __builtin_mul_overflow_p (a, b, (__typeof__ ((a) * (b))) 0)

void *beware_overflow(void *ptr, size_t nmemb, size_t size)
{
    if (OVERFLOW(nmemb, size))
        return NULL;
    char *pt = ptr;
    return pt + (nmemb * size);
}
