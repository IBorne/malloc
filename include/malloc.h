#pragma once

#define DEFAULT_SOURCE

#include <stddef.h>
#include <sys/mman.h>
#include <string.h>
#include <stdint.h>
#include <err.h>
#include <unistd.h>

#define PAGE_SIZE sysconf(_SC_PAGE_SIZE)

size_t *to_size(void *ptr);
void *to_void(void *ptr);
size_t word_align(size_t n);
size_t page_align(size_t n);
void *shift(void *ptr, size_t offset);
size_t ovf(size_t a, size_t b);
void *page_begin(void *ptr, size_t page_size);

struct block
{
    int security;
    struct block *next;
    int free:2;
    size_t size;
};

struct page
{
    struct page *next;
    size_t capacity;
    size_t size;
    struct block *meta;
};

struct page *page;

struct page *page_init(size_t size);
struct block *block_add(struct page *page, size_t size);
struct block *block_find(struct page *page, size_t size);
struct block *block_find_free(struct page *page, void *ptr, struct page **f_p);
int page_count_block(struct page *page);
void page_connect(struct page *page_begin, struct page *f_p);
void page_free(struct page *page);
